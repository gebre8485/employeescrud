import {call, put, takeEvery} from "redux-saga/effects";
import { GET_EMPLOYEES } from "../constants/actionTypes";
import { Employee, EMPLOYEE_GET_DATA } from "../constants/dataTypes";
import {GET_EMPLOYEES_FAILED, GET_EMPLOYEES_SUCCESS} from "../constants/actionTypes"
import axios from "axios";

const apiURL = "http://localhost:5000/employees";

function getAPI(){

    return axios.get(apiURL).then(response => response).catch(err => {
        throw err;
    })
    // return fetch(apiURL, {
    //     method:"GET",
    //     headers: {
    //         "Content-Type": "application/json"
    //     }
    // }).then(response =>response.json()).catch(error => {
    //     throw error});
}

function* fetchEmployees(action:EMPLOYEE_GET_DATA){
        try {
            const res = yield call(getAPI);
            const employees:Employee[] = res;
            yield put({type: GET_EMPLOYEES_SUCCESS, payload: res.data });
        } catch (err) {
            yield put({type: GET_EMPLOYEES_FAILED, payload: err.response.data.errors});
        }
}

function* employeeSaga(){
    yield takeEvery(GET_EMPLOYEES, fetchEmployees)
}

export default employeeSaga;