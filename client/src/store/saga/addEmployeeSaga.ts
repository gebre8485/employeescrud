import {call, delay, put, takeEvery} from "redux-saga/effects";
import { ADD_EMPLOYEE, ADD_EMPLOYEE_FAILED, ADD_EMPLOYEE_SUCCESS, REMOVE_ERRORS } from "../constants/actionTypes";
import { Employee, EMPLOYEE_ADD_DATA, EMPLOYEE_GET_DATA } from "../constants/dataTypes";
import {GET_EMPLOYEES_FAILED, GET_EMPLOYEES_SUCCESS} from "../constants/actionTypes"
import axios from "axios";
import { removeErrors } from "../actions/action";
import {useDispatch} from "react-redux";
import { wait, waitFor } from "@testing-library/react";

const apiURL = "http://localhost:5000/employees";

function getAPI(payload:Employee){
        return axios.post(apiURL, payload).then(response =>response).catch(error => {
            throw error;    
        })

    // return fetch(apiURL, {
    //     method:"POST",
    //     body: JSON.stringify(payload),
    //     headers: {
    //         "Content-Type": "application/json"
    //     }
    // }).then(response =>response.json()).catch(error => {throw error});
}

function* addEmployee(action:EMPLOYEE_ADD_DATA){
        const {payload} = action;
        try {
            const res = yield call(getAPI, payload);
            console.log("add AddEmployee saga ", res.data);
            yield put({type: ADD_EMPLOYEE_SUCCESS, payload: res.data });
        } catch (error) {
            console.log("addemployeeSaga: the error is ", error.response.data.errors)
            yield put({type: ADD_EMPLOYEE_FAILED, payload: error.response.data.errors});
            yield delay(5000);
            yield put({type: REMOVE_ERRORS});
            ;
        }
}

function* employeeSaga(){
    yield takeEvery(ADD_EMPLOYEE, addEmployee)
}

export default employeeSaga;