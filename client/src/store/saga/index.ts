import {all} from "redux-saga/effects"
import employeeSaga from "./employeeSaga"
import addEmployeeSaga from "./addEmployeeSaga"
import editEmployeeSaga from "./editEmployeeSaga"
import deleteEmployeeSaga from "./deleteEmployeeSaga"

export default function* rootSaga(){
    yield all([
        employeeSaga(),
        addEmployeeSaga(),
        editEmployeeSaga(),
        deleteEmployeeSaga()
    ])
}