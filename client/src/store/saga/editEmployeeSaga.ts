import {call, delay, put, takeEvery} from "redux-saga/effects";
import { ADD_EMPLOYEE, ADD_EMPLOYEE_FAILED, ADD_EMPLOYEE_SUCCESS, EDIT_EMPLOYEE, REMOVE_ERRORS, EDIT_EMPLOYEE_SUCCESS
    ,EDIT_EMPLOYEE_FAILED } from "../constants/actionTypes";
import { Employee, EmployeeEditPayload, EMPLOYEE_ADD_DATA, EMPLOYEE_EDIT_DATA } from "../constants/dataTypes";
import axios from "axios";

const apiURL = "http://localhost:5000/employees";

function getAPI(payload:EmployeeEditPayload){
        const data = {salary: payload.salary};
        return axios.put(apiURL + "/" + payload.id, data).then(response =>response).catch(error => {
            throw error;    
        })

}

function* editEmployee(action:EMPLOYEE_EDIT_DATA){
        const {payload} = action;
        try {
            console.log("editEmployeeSaga: the payload is ", payload);
            const res = yield call(getAPI, payload);
            console.log("EditEmployee saga ", res.data);
            yield put({type: EDIT_EMPLOYEE_SUCCESS, payload: res.data });
        } catch (error) {
            console.log("EditEmployeeSaga: the error is ", error.response.data.errors)
            yield put({type: EDIT_EMPLOYEE_FAILED, payload: error.response.data.errors});
            yield delay(5000);
            yield put({type: REMOVE_ERRORS});
            ;
        }
}

function* employeeSaga(){
    yield takeEvery(EDIT_EMPLOYEE, editEmployee)
}

export default employeeSaga;