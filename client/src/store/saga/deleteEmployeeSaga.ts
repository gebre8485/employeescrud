import {call, delay, put, takeEvery} from "redux-saga/effects";
import { ADD_EMPLOYEE, ADD_EMPLOYEE_FAILED, ADD_EMPLOYEE_SUCCESS, EDIT_EMPLOYEE, REMOVE_ERRORS, EDIT_EMPLOYEE_SUCCESS
    ,EDIT_EMPLOYEE_FAILED, 
    DELETE_EMPLOYEE,
    DELETE_EMPLOYEE_SUCCESS,
    DELETE_EMPLOYEE_FAILED} from "../constants/actionTypes";
import { Employee, EmployeeEditPayload, EMPLOYEE_ADD_DATA, EMPLOYEE_EDIT_DATA } from "../constants/dataTypes";
import axios from "axios";

const apiURL = "http://localhost:5000/employees";

function getAPI(payload:EmployeeEditPayload){
        return axios.delete(apiURL + "/" + payload).then(response =>response).catch(error => {
            throw error;    
        })

}

function* deleteEmployee(action:EMPLOYEE_EDIT_DATA){
        const {payload} = action;
        try {
            console.log("deleteEmployeeSaga: the payload is ", payload);
            const res = yield call(getAPI, payload);
            console.log("EditEmployee saga ", res.data);
            yield put({type: DELETE_EMPLOYEE_SUCCESS, payload });
        } catch (error) {
            console.log("deleteEmployeeSaga: the error is ", error.response.data.errors)
            yield put({type: DELETE_EMPLOYEE_FAILED, payload: error.response.data.errors});
            yield delay(5000);
            yield put({type: REMOVE_ERRORS});
            ;
        }
}

function* employeeSaga(){
    yield takeEvery(DELETE_EMPLOYEE, deleteEmployee)
}

export default employeeSaga;