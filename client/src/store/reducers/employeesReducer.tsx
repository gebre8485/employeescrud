import { ADD_EMPLOYEE, ADD_EMPLOYEE_FAILED, ADD_EMPLOYEE_SUCCESS, DELETE_EMPLOYEE_FAILED, DELETE_EMPLOYEE_SUCCESS, EDIT_EMPLOYEE_FAILED, EDIT_EMPLOYEE_SUCCESS, GET_EMPLOYEES_FAILED, GET_EMPLOYEES_SUCCESS, REMOVE_ERRORS, } from "../constants/actionTypes";
import { Employee, Employees, EMPLOYEE_ADD_DATA, Error, GetEmployeesFailedAction, EmployeesState, GetEmployeesSuccessAction} from "../constants/dataTypes";


   const initialState: EmployeesState = {
       employees:[],
       errors: [],
       loading: true
   };

export default function(state:EmployeesState = initialState, action:GetEmployeesFailedAction | any){
    const {type, payload} = action;
    let newState: EmployeesState = {...state}
    const{employees, errors, loading} = newState;
    switch (type) {
        case GET_EMPLOYEES_FAILED:
        case ADD_EMPLOYEE_FAILED:
        case EDIT_EMPLOYEE_FAILED:
        case DELETE_EMPLOYEE_FAILED:
            newState.errors = [...payload];
            newState.loading = false;
            break;
        case GET_EMPLOYEES_SUCCESS:
            newState.employees = [...payload];
            newState.loading = false;
            break;
        case ADD_EMPLOYEE_SUCCESS:
            newState.employees.unshift(payload);
            newState.loading = false;
            break;
        case REMOVE_ERRORS:
            newState.errors = [];
            break;
        case EDIT_EMPLOYEE_SUCCESS:
            newState.employees = newState.employees.map(employee =>{
                if (employee._id == payload._id){
                    employee.salary = payload.salary;
                }
                return employee;
            })
            break;
        case DELETE_EMPLOYEE_SUCCESS:
            newState.employees = newState.employees.filter(employee => {
                if (employee._id != payload){
                    return employee;
                }
            })
            break;
        default:
            break;
    }
    return newState;
}