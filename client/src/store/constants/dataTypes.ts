import { type } from "os";

export interface Employee{
    _id?: string
    name: string,
    dateOfBirth: string;
    gender: string,
    salary: number
};
export type ADD_EMPLOYEE_FUNC = (employee:Employee) => {
    type: string,
    payload: Employee
};
export type EMPLOYEE_ADD_DATA = {
    type: string,
    payload: Employee
}

export type EMPLOYEE_GET_DATA = {
    type:string;
}
export type EmployeeEditPayload ={
    id: string | undefined;
    salary: string;
}
export type EMPLOYEE_EDIT_DATA = {
    type: string;
    payload: EmployeeEditPayload;
}

export type Employees = Employee[];
export type Error = {msg:string};

export type GetEmployeesFailedAction = {
    type: string;
    payload: Error[];
}
export type GetEmployeesSuccessAction = {
    type: string;
    payload: Employees[];
}

export type EmployeesState = {
    employees: Employee[],
    errors:Error[],
    loading: boolean
}
export type State = {
    employees: EmployeesState,
}