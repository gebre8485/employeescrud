import {createStore, applyMiddleware, compose} from 'redux';
import { Employee } from './constants/dataTypes';
import allReducers from "./reducers/allReducers"
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleWare from "redux-saga"
import rootSaga from "./saga"


// export interface EmployeesState {
//     employees : Employee[];
// }
const sagaMiddleware = createSagaMiddleWare();
const appStore = compose(
    applyMiddleware(sagaMiddleware),
    composeWithDevTools()
)(createStore)(allReducers);
sagaMiddleware.run(rootSaga)
export { appStore };