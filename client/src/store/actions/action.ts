import {useDispatch} from "react-redux";

import { ADD_EMPLOYEE, DELETE_EMPLOYEE, EDIT_EMPLOYEE, REMOVE_ERRORS } from "../constants/actionTypes"
import { ADD_EMPLOYEE_FUNC, Employee, EmployeeEditPayload } from "../constants/dataTypes"



export const addEmployee: ADD_EMPLOYEE_FUNC = (employee) =>{
    return {
        type: ADD_EMPLOYEE,
        payload: employee
    }
}
export const removeErrors = () =>{
    setTimeout(() => {
     return {
        type: REMOVE_ERRORS
    }   
    }, 4000);   
}
export const editEmployee = (emp:EmployeeEditPayload) => {
    return{
        type: EDIT_EMPLOYEE,
        payload: emp
    }
}
export const deleteEmployee = (id: string | undefined) =>{
    return {
        type: DELETE_EMPLOYEE,
        payload: id
    }
}
