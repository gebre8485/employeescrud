import React, {useState} from "react"
import { Employee } from '../store/constants/dataTypes';
import styled from "styled-components";
import EditComponent from "./EditComponent"
import { useDispatch } from "react-redux";
import {deleteEmployee} from "../store/actions/action"
const Container = styled.div`
    border: 3px solid rgba(150, 150, 150, 0.5);
    box-shadow: 2px 2px 3px gray;
    margin-bottom:10px;
    `
const EmployeeContainer = styled.div`
    text-align: left;
    overflow: hidden;
    padding:10px;
    `
const DataContainer = styled.div`
    width: 80%;
    float: left;
    `
const ActionsContainer = styled.div`
    float: right;
    width: 15%;
    display: flex;
    flex-direction: column;
    height: 170px;
    justify-content: space-around;
    `
const DeleteButton = styled.button`
    padding: 5px;
    font-weight: bold;
    color: red;
    `
const EditButton = styled.button`
    padding: 5px;
    font-weight: bold;
    color: teal;
    `
interface EmployeeProps{
    employee: Employee;
}
const EmployeeComponent: React.FC<EmployeeProps> = ({employee}) => {
    let {name, dateOfBirth, gender, salary} = employee;
    dateOfBirth = dateOfBirth.substring(0, 10);
    const dispatch = useDispatch();
    const [edit, setedit] = useState(false);
    const hideEditForm = () => {
        setedit(false);
    }
    return (
        <Container>
        <EmployeeContainer>
            <DataContainer>
            <h3>Name: {name} </h3>
            <p>Date of Birth: {dateOfBirth}</p>
            <p>Gender: {gender} </p>
            <p>Salary: {salary} </p>  
            </DataContainer>
            <ActionsContainer>
            <EditButton onClick={() => setedit(true)}>edit</EditButton>
            <DeleteButton onClick={() => dispatch(deleteEmployee(employee._id))}>delete</DeleteButton>                
            </ActionsContainer>
        </EmployeeContainer>
            {edit && <EditComponent id={employee._id} salary={employee.salary} hideEditForm={hideEditForm} />}
        </Container>
        
    )
}
export default EmployeeComponent

