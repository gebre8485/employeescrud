import React, {ChangeEvent, FormEvent, useState} from 'react'
import { useDispatch } from 'react-redux'
import {editEmployee} from "../store/actions/action";
interface EditProps{
    id:string | undefined;
    salary:number;
    hideEditForm:()  =>void
}
const EditComponent: React.FC<EditProps> = ({id, salary, hideEditForm}) => {
    const dispatch = useDispatch();
    const [formData, setformData] = useState(salary.toString())
    const changeHandler = (e:ChangeEvent<HTMLInputElement>) => {
        setformData(e.target.value);
    }
    
    const submitHandler = (e:FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        dispatch(editEmployee({id, salary: formData}));
        hideEditForm();
    }

    return (
        <form onSubmit={submitHandler}>
            <label>Edit Salary: </label>
            <input type="number" name="salary" value={formData} onChange={changeHandler}/>
            <input type="submit" value="Submit"/>
        </form>
    )
}
export default EditComponent;
