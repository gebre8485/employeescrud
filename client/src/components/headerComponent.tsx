import React from 'react'
import styled from "styled-components";

const Top = styled.header`
    font-family: ${(props) => (props.theme.font)};
    background: #232323;
    min-height:100px;
    `
const Header = styled.header`
    text-align: center;
    color: #bcbcbc;
    padding: 20px;
`
const Exercise = () => {
    return (
        <Top>
            <Header>
                <h1 style={{marginTop:"0"}}>Employee Management</h1>
                <p>Create, Read, Update and Delete employees here.</p>
            </Header>
        </Top>
    )
}

export default Exercise;
