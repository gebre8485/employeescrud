import React, {FC, useEffect} from 'react'
import PropTypes from 'prop-types'
import {useDispatch, useSelector} from "react-redux";
import { State } from '../store/constants/dataTypes';
import {EmployeesState} from "../store/constants/dataTypes";
import { type } from 'os';
import EmployeeComponent from "./employeeComponent"
import { GET_EMPLOYEES } from '../store/constants/actionTypes';
import styled from "styled-components";

const Container = styled.div`
    flex-grow: 10;
    box-sizing: border-box;
    padding: 10px;
    text-align: center;
    `
const ErrorContainer = styled.p`
    color: red;
    font-size: 2rem;
    `
const EmployeesContainer = styled.div`
    padding: 0px;
    `

const EmployeesComponent: FC = () =>{
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch({type:GET_EMPLOYEES})
    }, []);
    const {employees, errors, loading} = useSelector((state: State) => state.employees);
    //const errorDisplay = 
    const employeesDisplay = employees.length === 0 ?<ErrorContainer>there are no employees!</ErrorContainer> : 
    <EmployeesContainer>
        {employees.map(employee => {
            return <EmployeeComponent employee={employee}/>
        })}
    </EmployeesContainer>
    return (
        <Container>
            {employeesDisplay}
        </Container>
    );
}

EmployeesComponent.propTypes = {

}
// const mapStateToProps = (state: EmployeesState) => ({
//     employees: state.employees,
//     errors: state.errors,
// })

export default EmployeesComponent;