import React, { ChangeEvent, FormEvent } from 'react'
import {useDispatch, useSelector} from "react-redux";
import {useState} from "react"
import {Employee, State} from "../store/constants/dataTypes"
import { addEmployee } from '../store/actions/action';
import styled from "styled-components";

const Container = styled.div`
  flex-grow: 1;
  padding: 10px;
  box-sizing: border-box;
  border-left: 3px solid #ddd;
  `
  const Description = styled.p`
    color: #04ff55;
    font-weight: bold;
    font-size 1.5em;
    width: 100%;
    `
  const Table = styled.table`
    background: #ccc;
    padding: 10px;
    width: 100%;
    `
  const Input = styled.input`
    padding-top: 10px;
    width: 100%;
    `
  const Label = styled.label`
      font-weight: bold;
      text-align: right;
    `
  const SubmitContainer = styled.div`
    display:flex;
    justify-content: flex-end
      `
  const SubmitButton = styled.input`
    padding:2px;
    text-weight: bold;
    color:teal;
    width: 100%;
    margin-top: 5px;
    font-size:1.2em;
    `
interface AddNewEmployeeProps{
  employee?: Employee,
  edit?:boolean
}

const AddNewEmployeeComponent: React.FC<AddNewEmployeeProps> = ({employee, edit}) => {
    if (employee){
      console.log("addnewEmployeeComponent: the employee is ", employee)
    }else{
      console.log("addnewEmployeeComponent: the employee must be undefined. ", employee) 
    }
    const dispatch = useDispatch();
    const {errors} = useSelector((state: State) => state.employees)
    const [formData, setformData] = useState({
        name:"",
        dateOfBirth:"",
        gender: "",
        salary: 0,
    })
    let {name, dateOfBirth, gender, salary} = formData;
    const changeHandler = (e:ChangeEvent<HTMLInputElement>) => {
        setformData({ ...formData, [e.target.name]: e.target.value })
    }
    const submitHandler = (e: FormEvent<HTMLFormElement>) =>{
        e.preventDefault();
        dispatch(addEmployee(formData));
        setformData({
          name:"",
          dateOfBirth:"",
          gender: "",
          salary: 0,
      })
    }
    return (
    <Container>
      <Description >Add new Employee</Description>
      <form onSubmit={submitHandler}>
        <Table>
          <tr>
            <td>
              <Label>Name:</Label>
            </td>
            <td>
            <Input type="text" placeholder="Enter name" name="name" value={name} onChange={changeHandler} />            
            </td>
          </tr>
          <tr>
            <td>
              <Label >Date of Birth: </Label> 
            </td>
            <td>
            <Input type="date" placeholder="Enter date of birth" name="dateOfBirth" value={dateOfBirth} onChange={changeHandler} />
            </td>
          </tr>
          <tr>
            <td>
          <Label >Gender: </Label> 
            </td>
            <td>
          <Input type="text" placeholder="Enter gender" name="gender" value={gender} onChange={changeHandler} />
            </td>
          </tr>
          <tr>
            <td>
          <Label >Salary: </Label>
            </td>
            <td>
          <Input type="number"placeholder="Enter salary" name="salary" value={salary} onChange={changeHandler} />
            </td>
          </tr>
          <td>

          </td>
        </Table>
        <SubmitContainer>
        <SubmitButton type="submit" value="Submit" />
        </SubmitContainer>
      </form>
    </Container> 
    )
}

export default AddNewEmployeeComponent

