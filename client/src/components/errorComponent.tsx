import React, { Fragment, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {State} from "../store/constants/dataTypes"
import {removeErrors} from "../store/actions/action";
import styled from "styled-components";

const Alert = styled.div`
    text-align:center;
    color: white;
    background: red;
    padding 5px;
    width: 50%;
    box-shadow:4px 4px 5px gray;
    margin: 10px auto;
    `
export default function ErrorComponent() {
    const {errors} = useSelector((state: State) => state.employees);
    console.log("ErrorComponent: the errors are ", errors);
    const alert = errors.length > 0 && (errors.map(error => (<Alert key={error.msg}>{error.msg}</Alert>)));
    return (
        <Fragment>
            {alert}
        </Fragment>
    )
}
