import React from 'react';
import './App.css';
import EmployeesComponent from "./components/employeesComponent"
import AddNewEmployeeComponent from "./components/addNewEmployeeComponent"
import {ThemeProvider} from "styled-components"
import {Provider} from "react-redux"
import {appStore} from "./store/store"
import HeaderComponent from "./components/headerComponent"
import Exercise from "./components/exercise"
import styled from "styled-components";
import ErrorComponent from "./components/errorComponent";

const theme = {
  font: "sans-serif;"
}

const Container = styled.div`
  display: flex;
  flex-direction: row;
  min-height: 80vh;
  padding-top: 40px;
`

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <HeaderComponent/>
      <ErrorComponent/>
      <Container>
      <div style={{flexGrow:2}}>
      </div>
      <EmployeesComponent/>
      <AddNewEmployeeComponent/>
      </Container>
            {/* <Exercise/> */}
      
    </ThemeProvider>
    

  );
}

export default App;
