const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  dateOfBirth: {
    type: Date,
    require: true,
    max: new Date()
  },
  gender: {
    type: String,
    required: true,
    enum: ["male", "female", "other"]
  },
  salary: {
      type: Number,
      required: true,
  }
});

module.exports = User = mongoose.model("employee", userSchema);
