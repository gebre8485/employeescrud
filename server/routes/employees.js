const express= require("express");
const {check, validationResult} = require("express-validator");
const Employee = require("../models/employee")

const router = express.Router();
//creting an employee.
router.post("/",  [
        check("name", "name is required.").not().isEmpty(),
        check("dateOfBirth", "the birth date is invalid").isDate(),//isBefore(new Date().toString()),
        check("gender", "The gender must be male, female or other.").isIn(["male", "female", "other"]),
        check("salary", "the salary must be a floating point greater than 0.").isFloat({ gt: 0.0 }),
        ],
   async(req, res) =>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array()});
    }
    const {name, dateOfBirth, gender, salary} = req.body;
    try {
        let employee = await Employee.findOne({name});
        if (employee) {
            return res.status(400).json({errors: [{msg: "the employee already exists."}]});
        }
        employee = new Employee({name, dateOfBirth, gender, salary});
        await employee.save();
        return res.status(200).json(employee)
   
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
      }

});

//listing employees.
router.get("/",async(req, res) =>{
        let employees = await Employee.find();
        if (employees.length === 0) {
            return res.status(400).json({errors: [{msg: "you have no employees yet."}]});
        }
        return res.status(200).json(employees);
});

//listing employees.
router.put("/:id",[
    check("salary", "the salary must be a floating point greater than 0.").isFloat({gt: 0.0}),
    ], async(req, res) =>{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array()});
          }
    const {id} = req.params;
    const {salary} = req.body;
    try {
    let employee = await Employee.findById(id);
    if (!employee) {
        return res.status(400).json({errors: [{msg: "no such employee."}]});
    }
    let employeeFields = {};
    if(salary) employeeFields.salary = salary;
    employee = await Employee.findOneAndUpdate(
        { _id: id },
        { $set: employeeFields },   
        { new: true, upsert: true }
    );
    res.json(employee);
    } catch (err) {
    res.status(500).json("server error.");
    }
});

//listing employees.
router.delete("/:id",async(req, res) =>{
    const {id} = req.params;
    try {
        let employee = await Employee.findById(id);
        if (!employee) {
            return res.status(400).json({errors: [{msg: "there is no such employee"}]});
        }
        await Employee.findByIdAndRemove(id);
        return res.status(200).json({msg: "user deleted."});  
    }  catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
      }

});
module.exports = router;